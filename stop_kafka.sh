#!/bin/bash

CID=$(docker ps | grep spotify\/kafka | awk '{print $1}');

docker kill ${CID}
