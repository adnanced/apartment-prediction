#!/bin/bash

CID=$(docker ps | grep spotify\/kafka | awk '{print $1}');

docker exec -it ${CID} /opt/kafka_2.11-0.10.1.0/bin/kafka-console-producer.sh --topic apartments --broker-list localhost:9092
