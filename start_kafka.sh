#!/bin/bash

CID=$(docker run -it -d -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=localhost --env ADVERTISED_PORT=9092 spotify/kafka);
sleep 4
docker exec -it ${CID} /opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --create --topic apartments --zookeeper localhost --replication-factor 1 --partitions 8
