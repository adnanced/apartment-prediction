name := "apartment-prediction"

scalaVersion := "2.11.11"

val sparkVersion = "2.1.1"
val sparkDependencies = Seq(
	"org.apache.spark" %% "spark-core",
	"org.apache.spark" %% "spark-mllib",
	"org.apache.spark" %% "spark-streaming",
  "org.apache.spark" %% "spark-streaming-kafka-0-10"
).map(_ % sparkVersion)

libraryDependencies ++= Seq(
	"com.typesafe" % "config" % "1.3.1",
  "com.typesafe.akka" %% "akka-http" % "10.0.6"
) ++ sparkDependencies
