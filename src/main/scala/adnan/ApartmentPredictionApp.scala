package adnan

import adnan.regression.LinearRegression
import adnan.stream.KafkaStream
import adnan.web.PredictionEndpoint
import com.typesafe.config.ConfigFactory

class ApartmentPredictionApp extends KafkaStream with LinearRegression with PredictionEndpoint {
  lazy val config = ConfigFactory.load

  def start(): Unit = {
    startHttpServer()
    startStream()
  }

}
