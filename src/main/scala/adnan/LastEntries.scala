package adnan

import org.apache.spark.mllib.regression.LabeledPoint

import scala.collection.mutable

object LastEntries {

  private[this] val stackLimit = 5

  val stack = mutable.Stack[LabeledPoint]()

  def push(item: LabeledPoint): LabeledPoint = {
    if(stack.length > stackLimit) stack.update(stackLimit - 1, item) else stack.push(item)
    item
  }

}
