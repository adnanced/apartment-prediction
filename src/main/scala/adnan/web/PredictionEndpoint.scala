package adnan.web

import adnan.LastEntries
import adnan.regression.LinearRegression
import adnan.util.AppConfig
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer

trait PredictionEndpoint extends AppConfig {
  self: LinearRegression =>

  private final case class Input(
                          size: Double,
                          nrRooms: Double,
                          floor: Double,
                          location: Double
                          ) {
    def asArray(): Array[Double] = Array(size, nrRooms, floor, location)
  }

  private[this] val route =
    path("apartment") {
      get {
        parameters(
          'size.as[Double],
          'rooms.as[Double],
          'floor.as[Double],
          'location.as[Double]
        ).as(Input) { input =>
          complete {
            val prediction = predict(input.asArray())
            f"$prediction%2.2f (~$accuracy%2.2f%%)"
          }
        }
      }
    }

  def startHttpServer(): Unit = {
    implicit val actorSystem = ActorSystem(config.getString("appName"))
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = actorSystem.dispatcher

    Http().bindAndHandle(route, config.getString("http.host"), config.getInt("http.port"))
  }

}
