package adnan.stream

import adnan.util.SparkConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.dstream.{ConstantInputDStream, DStream}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent

import scala.util.Random

trait KafkaStream extends SparkConfig {

  private lazy val kafkaParams = {
    val rnd = new Random()

    val deserializer = classOf[StringDeserializer]
    val brokers = config.getString("kafka.brokers")
    val groupId = s"${config.getString("kafka.groupId")}-${rnd.nextInt(10000)}"

    Map[String, Object](
      "bootstrap.servers" -> brokers,
      "key.deserializer" -> deserializer,
      "value.deserializer" -> deserializer,
      "group.id" -> groupId,
      "auto.offset.reset" -> "earliest",
      "enable.auto.commit" -> (true: java.lang.Boolean)
    )
  }

  def createStream(): DStream[ConsumerRecord[String, String]] = {
    val topics = Array(config.getString("kafka.topic"))
    KafkaUtils.createDirectStream[String, String](context, PreferConsistent, Subscribe[String, String](topics, kafkaParams))
  }

  def startStream(): Unit = {
    context.start()
    context.awaitTermination()
  }

}
