package adnan

object Main {

  def main(args: Array[String]): Unit = {
    val app = new ApartmentPredictionApp

    app.trainModel(app.createStream())

    app.start()
  }

}
