package adnan.regression

import adnan.LastEntries
import adnan.util.AppConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.{Vector => MLVector}
import org.apache.spark.mllib.regression.{LabeledPoint, StreamingLinearRegressionWithSGD}
import org.apache.spark.streaming.dstream.DStream

trait LinearRegression extends AppConfig {

  val nrFeatures = 4

  lazy val algorithm = {
    val algo = new StreamingLinearRegressionWithSGD()
    algo.setStepSize(config.getDouble("regression.step"))
      .setNumIterations(config.getInt("regression.iteration"))
      .setInitialWeights(Vectors.zeros(nrFeatures))
    algo.algorithm.setIntercept(true)
    algo
  }

  def trainModel(stream: DStream[ConsumerRecord[String, String]]): Unit = {
    val trainingData = stream.map { record =>
      val data = record.value().split(',').map(_.toDouble)
      val point = LabeledPoint(data.head, Vectors.dense(data.tail))
      LastEntries.push(point)
    }
    algorithm.trainOn(trainingData)
  }

  def accuracy() = LastEntries.stack.map { lp =>
    math.abs(predict(lp.features) - lp.label) / lp.label
  }.sum / LastEntries.stack.length

  private[this] def predict(features: MLVector): Double = algorithm.latestModel().predict(features)

  def predict(features: Array[Double]): Double = predict(Vectors.dense(features))

}
