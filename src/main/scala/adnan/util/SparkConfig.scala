package adnan.util

import org.apache.spark.SparkConf
import org.apache.spark.streaming._

trait SparkConfig extends AppConfig {

  lazy val context = {
    val appName = config.getString("appName")
    val master = config.getString("spark.master")
    val batchDuration = config.getDuration("spark.batchDuration").toMillis

    val conf = new SparkConf().setAppName(appName).setMaster(master)
    new StreamingContext(conf, Milliseconds(batchDuration))
  }

}
