package adnan.util

import com.typesafe.config.Config

trait AppConfig {

  def config: Config

}
