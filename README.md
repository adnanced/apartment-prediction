# README #

This is simple implementation of the machine learning to predict apartment prices based on the simple model. I used linear regression with Spark to train the model and test data is consumed from Kafka topic. To access the prediction model you can use `/apartment` endpoint (on port 8000) which is implemented with Akka-Http.

It's possible to use pre-setup Kafka cluster with 3 shell script provided (docker is required).